/*
 * Lightbridge  Kernel implementation of the Lightbridge system for interacting
 * 		with switched optical networks
 *
 * Authors:	Ryan Goodfellow <rgoodfel@isi.edu>
 *		Yiwen Shen <ys2799@columbia.edu>
 *
 *		This program is free software; you can redistribute it and/or
 *		modify it under the terms of the GNU General Public License
 *		as published by the Free Software Foundation; either version
 *		2 of the License, or (at your option) any later version.
 */
#ifndef _LINUX_IF_LIGHTBRIDGE_H_
#define _LINUX_IF_LIGHTBRIDGE_H_

#include <linux/skbuff.h>

#if defined(CONFIG_LIGHTBRIDGE) || defined(CONFIG_LIGHTBRIDGE_MODULE)

extern bool lb_do_receive(struct sk_buff **skbp);

#else

static inline bool lb_do_receive(struct sk_buff **skb)
{
	return false;
}

#endif

#endif
