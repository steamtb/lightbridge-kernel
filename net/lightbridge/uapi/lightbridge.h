// SPDX-License-Identifier: (GPL-2.0 WITH Linux-syscall-note) OR MIT
/*
 * Copyright (C) 2019 Information Sciences Institute. All Rights Reserved.
 */
#ifndef _LB_UAPI_LIGHTBRIDGE_H
#define _LB_UAPI_LIGHTBRIDGE_H

#include <linux/netdevice.h>

#define LB_GENL_NAME "lightbridge"
#define LB_GENL_VERSION 1

enum lb_cmd {
	LB_CMD_GET_DEVICE,
	LB_CMD_SET_DEVICE,
	__LB_CMD_MAX
};
#define LB_CMD_MAX (__LB_CMD_MAX - 1)

enum lbdevice_attribute {
	LBDEVICE_A_UNSPEC,
	LBDEVICE_A_IFINDEX,
	LBDEVICE_A_IFNAME,
	LBDEVICE_A_CIRCUIT,
	LBDEVICE_A_PARENT,
	LBDEVICE_A_FLAGS,
	__LBDEVICE_A_LAST
};
#define LBDEVICE_A_MAX (__LBDEVICE_A_LAST - 1)

struct lightbridge_private;

static inline struct lightbridge_private *lb_dev_priv(const struct net_device *dev)
{
  return netdev_priv(dev);
}


#endif /* _LB_UAPI_LIGHTBRIDGE_H */
