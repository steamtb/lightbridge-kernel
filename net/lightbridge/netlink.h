// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (C) 2019 Information Sciences Institute. All Rights Reserved.
 */

#ifndef _LB_NETLINK_H
#define _LB_NETLINK_H

int lb_genetlink_init(void);
void lb_genetlink_uninit(void);

#endif /* _LB_NETLINK_H */
